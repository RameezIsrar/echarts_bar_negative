<?php

class Data { 
    public $aMemberVar ; 
    public $aFuncName = 'aMemberFunc'; 
    
    
    function aMemberFunc() { 
     	$days_in_week = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
     	$days_in_week_json = json_encode($days_in_week);
     
     	$week_days_data = [200, 170, 240, 244, 200, 220, 210];
		$week_days_data_json = json_encode($week_days_data);
		
		$more_data = [320, 302, 341, 374, 390, 450, 420];
		$more_data_json = json_encode($more_data);

		$cool_data = [-120, -132, -101, -134, -190, -230, -210];
		$cool_data_json = json_encode($cool_data);

		$date_postion_occurance = array();
		for ($i=0; $i < 29; $i++) { 
			$date_postion_occurance[$i]= [rand(0,6),rand(0,29),rand(0,15)];
		}
		$date_postion_occurance_json =json_encode($date_postion_occurance); 
		
	   //===========================  pulling js  =================== 
        
        $jscript_to_inject = array();
		  
     	$jscript_to_inject[] = "<script type='text/javascript' src='assets/js/echarts-all-3.js'></script>";
	    $jscript_to_inject[] = "<script type='text/javascript' src='assets/js/ecStat.min.js'></script>";
		$jscript_to_inject[] = "<script type='text/javascript' src='assets/js/assets/js/dataTool.min.js'></script>";
		$jscript_to_inject[] = "<script type='text/javascript' src='assets/js/china.js'></script>";
		$jscript_to_inject[] = "<script type='text/javascript' src='assets/js/world.js'></script>";
		$jscript_to_inject[] = "<script type='text/javascript' src='http://api.map.baidu.com/api?v=2.0&ak=ZUONbpqGBsYGXNIYHicvbAbM'></script>";
		$jscript_to_inject[] = "<script type='text/javascript' src='assets/js/bmap.min.js'></script>";


        $jscript_to_inject[] = "<script type='text/javascript'>";

		$jscript_to_inject[] =	"var dom = document.getElementById('container');";
		$jscript_to_inject[] =	"var myChart = echarts.init(dom);";
		$jscript_to_inject[] = "var app = {};";
		$jscript_to_inject[] = "option = null;";
		$jscript_to_inject[] = "app.title = '正负条形图';" ;

		$jscript_to_inject[] = "option = {" ;
		$jscript_to_inject[] =    "tooltip : {" ;
		$jscript_to_inject[] =        "trigger: 'axis'," ;
		$jscript_to_inject[] =        "axisPointer : {            // 坐标轴指示器，坐标轴触发有效" ;
		$jscript_to_inject[] =            "type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow' ";
		$jscript_to_inject[] =       " }";
		$jscript_to_inject[] =   " },";
		$jscript_to_inject[] =   "legend: {";
		$jscript_to_inject[] =      "  data:['profit', 'expenditure', 'income']";
		$jscript_to_inject[] =   " },";
		$jscript_to_inject[] =   " grid: {";
		$jscript_to_inject[] =      "left: '3%',";
		$jscript_to_inject[] =     "right: '4%',";
		$jscript_to_inject[] =        "bottom: '3%',";
		$jscript_to_inject[] =        "containLabel: true";
		$jscript_to_inject[] =    "},";
		$jscript_to_inject[] =    "xAxis : [";
		$jscript_to_inject[] =       " {";
		$jscript_to_inject[] =            "type : 'value'";
		$jscript_to_inject[] =       "}";
		$jscript_to_inject[] =    "],";
		$jscript_to_inject[] =    "yAxis : [";
		$jscript_to_inject[] =       "{";
		$jscript_to_inject[] =            "type : 'category',";
		$jscript_to_inject[] =            "axisTick : {show: false},";
		$jscript_to_inject[] =            "data : $days_in_week_json";
		$jscript_to_inject[] =       " }";
		$jscript_to_inject[] =    "],";
		$jscript_to_inject[] =    "series : [";
		$jscript_to_inject[] =       " {";
		$jscript_to_inject[] =           "name:'profit',";
		$jscript_to_inject[] =            "type:'bar',";
		$jscript_to_inject[] =            "label: {";
		$jscript_to_inject[] =                "normal: {";
		$jscript_to_inject[] =                   " show: true,";
		$jscript_to_inject[] =                    "position: 'inside'";
		$jscript_to_inject[] =                "}";
		$jscript_to_inject[] =            "},";
		$jscript_to_inject[] =            "data:$week_days_data_json";
		$jscript_to_inject[] =        "},";
		$jscript_to_inject[] =       "{";
		$jscript_to_inject[] =           " name:'income',";
		$jscript_to_inject[] =            "type:'bar',";
		$jscript_to_inject[] =            "stack: 'Total amount',";
		$jscript_to_inject[] =            "label: {";
		$jscript_to_inject[] =                "normal: {";
		$jscript_to_inject[] =                    "show: true";
		$jscript_to_inject[] =                "}";
		$jscript_to_inject[] =            "},";
		$jscript_to_inject[] =            "data: $more_data_json";
		$jscript_to_inject[] =        "},";
		$jscript_to_inject[] =       " {";
		$jscript_to_inject[] =           " name:'expenditure',";
		$jscript_to_inject[] =            "type:'bar',";
		$jscript_to_inject[] =            "stack: 'Total amount',";
		$jscript_to_inject[] =          "  label: {";
		$jscript_to_inject[] =               " normal: {";
		$jscript_to_inject[] =                   " show: true,";
		$jscript_to_inject[] =                   " position: 'left'";
		$jscript_to_inject[] =              " }";
		$jscript_to_inject[] =          " },";
		$jscript_to_inject[] =            "data:$cool_data_json";
		$jscript_to_inject[] =       "}";
		$jscript_to_inject[] =   "]";
		$jscript_to_inject[] = "};";
		$jscript_to_inject[] = ";";
		$jscript_to_inject[] = "if (option && typeof option === 'object') {";
		$jscript_to_inject[] =    "myChart.setOption(option, true);";
		$jscript_to_inject[] = "}";
					        
		$jscript_to_inject[]=	 "</script>";
			       
					         // 
     	return implode("\n", $jscript_to_inject);
		}
	}
$data = new Data; 
$var =  $data->aMemberFunc();
echo $var;

?>